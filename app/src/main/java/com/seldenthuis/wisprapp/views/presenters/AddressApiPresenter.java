package com.seldenthuis.wisprapp.views.presenters;

import com.seldenthuis.wisprapp.domain.Address;

public interface AddressApiPresenter
{

    void address(Address address);

    void onError(String errorMessage);

}
