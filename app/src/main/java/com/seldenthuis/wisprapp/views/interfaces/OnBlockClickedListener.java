package com.seldenthuis.wisprapp.views.interfaces;

import android.view.View;

import com.seldenthuis.wisprapp.domain.Block;

public interface OnBlockClickedListener
{
    void onClick(Block block);
}