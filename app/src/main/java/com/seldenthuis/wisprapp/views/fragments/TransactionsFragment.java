package com.seldenthuis.wisprapp.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seldenthuis.wisprapp.R;
import com.seldenthuis.wisprapp.domain.Block;
import com.seldenthuis.wisprapp.domain.Transaction;
import com.seldenthuis.wisprapp.network.block.BlockApi;
import com.seldenthuis.wisprapp.network.transaction.TransactionApi;
import com.seldenthuis.wisprapp.views.adapters.TransactionAdapter;
import com.seldenthuis.wisprapp.views.presenters.BlockApiPresenter;
import com.seldenthuis.wisprapp.views.presenters.TransactionApiPresenter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class TransactionsFragment extends Fragment implements BlockApiPresenter, TransactionApiPresenter
{

    public static String TAG = "seldenthuis";

    private BlockApi blockApi;

    private TransactionApi transactionApi;

    private RecyclerView transactionsRecyclerView;

    private TextView blockHeightTextView;

    private TextView blockHashTextView;

    private TextView blockConfirmations;

    private TextView blockTransactionsAmount;

    private TextView blockTimestamp;

    private TransactionAdapter transactionAdapter;

    private String blockHash;

    private Block block;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        Bundle bundle = getArguments();
        this.blockApi = new BlockApi(this);
        this.transactionApi = new TransactionApi(this);
        this.blockHash = bundle.getString("blockHash");
        // Inflate the layout for this previewFragment
        return inflater.inflate(R.layout.fragment_block,
                container,
                false);
    }

    @Override
    public void onViewCreated(View parent, Bundle savedInstance)
    {
        this.setupBlockAdapter();
        this.transactionsRecyclerView = parent.findViewById(R.id.block_transactions_recycler_view);
        this.blockHeightTextView = parent.findViewById(R.id.transaction_block_chain_height_text_view);
        this.blockHashTextView = parent.findViewById(R.id.transaction_block_hash_text_view);
        this.blockConfirmations = parent.findViewById(R.id.transaction_block_confirmations_text_view);
        this.blockTransactionsAmount = parent.findViewById(R.id.transaction_block_transactions_text_view);
        this.blockTimestamp = parent.findViewById(R.id.transaction_block_timestamp_text_view);

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.transactionsRecyclerView.setLayoutManager(llm);
        this.transactionsRecyclerView.setAdapter(this.transactionAdapter);
    }

    private void setupBlockAdapter()
    {
        this.transactionAdapter = new TransactionAdapter(new ArrayList<Transaction>());
    }

    @Override
    public void onStart()
    {
        super.onStart();
        this.getActivity().setTitle("Block transactions");

        // This starts the 'chain' of event by first fetching the highest block-index.
        this.blockApi.getByBlockHash(this.blockHash);

    }

    private void fillBlockWithData(Block block)
    {
        this.blockHeightTextView.setText(String.format("#%s", String.valueOf(block.height)));
        this.blockHashTextView.setText(
                String.format("[%s..%s]", block.hash.substring(0, 6), block.hash.substring(58, 64))
        );
        this.blockConfirmations.setText(String.valueOf(block.confirmations));
        this.blockTransactionsAmount.setText(String.valueOf(block.tx.length));
        this.blockTimestamp.setText(this.simpleDateFormat.format(block.time));
    }

    @Override
    public void block(final Block block)
    {
        Log.d(TAG, "block: " + block);
        getActivity().runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                TransactionsFragment.this.fillBlockWithData(block);
                for (String txId : block.tx) {
                    TransactionsFragment.this.transactionApi.getById(txId);
                }
            }
        });

    }

    @Override
    public void blockHash(String blockHash)
    {
        this.blockApi.getByBlockHash(blockHash);
    }

    @Override
    public void blockHeight(int blockHeight)
    {
        this.blockApi.getBlockHashByIndex(blockHeight);
    }

    @Override
    public void transaction(final Transaction transaction)
    {
        getActivity().runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                TransactionsFragment.this.transactionAdapter.addTransaction(transaction);
            }
        });
    }

    @Override
    public void onError(String errorMessage)
    {
        Log.d(TAG, "onError: " + errorMessage);
    }

}
