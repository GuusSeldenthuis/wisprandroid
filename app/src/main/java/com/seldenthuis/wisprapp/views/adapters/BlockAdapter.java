package com.seldenthuis.wisprapp.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.seldenthuis.wisprapp.R;
import com.seldenthuis.wisprapp.domain.Block;
import com.seldenthuis.wisprapp.views.interfaces.OnBlockClickedListener;

import java.text.SimpleDateFormat;
import java.util.List;

public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.ViewHolder>
{

    private List<Block> blocks;

    private AdapterPositionListener adapterPositionListener;

    private OnBlockClickedListener onBlockClickedListener;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

    public BlockAdapter(List<Block> blocks)
    {
        this.blocks = blocks;
    }

    public void setBlocks(List<Block> blocks)
    {
        this.blocks = blocks;
        this.notifyDataSetChanged();
    }

    public void addBlock(Block block)
    {
        this.blocks.add(block);
        this.notifyDataSetChanged();
    }

    public void addOnBlockClickedListener(OnBlockClickedListener onBlockClickedListener) {
       this.onBlockClickedListener = onBlockClickedListener;
    }

    public void setAdapterPositionListener(AdapterPositionListener adapterPositionListener)
    {
        this.adapterPositionListener = adapterPositionListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.block_row_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        final Block block = this.blocks.get(position);
        holder.blockHashTextView.setText(String.format("[%s..%s]", block.hash.substring(0, 6), block.hash.substring(58, 64)));
        holder.blockHeightTextView.setText(String.format("#%d", block.height));
        holder.blockTransactionsTextView.setText(String.format("%d", block.tx.length));
        holder.blockConfirmationsTextView.setText(String.format("%d", block.confirmations));
        holder.blockTimestampTextView.setText(this.simpleDateFormat.format(block.time));
        holder.blockContainerLinearLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                BlockAdapter.this.onBlockClickedListener.onClick(block);
            }
        });

        holder.itemView.setTag(block.hash);

        this.adapterPositionListener.adapterItemPosition(position);
    }

    @Override
    public int getItemCount()
    {
        return this.blocks.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {

        public LinearLayout blockContainerLinearLayout;
        public TextView blockHeightTextView;
        public TextView blockHashTextView;
        public TextView blockTransactionsTextView;
        public TextView blockConfirmationsTextView;
        public TextView blockTimestampTextView;

        public ViewHolder(View itemView)
        {
            super(itemView);
            this.blockContainerLinearLayout = (LinearLayout) itemView.findViewById(R.id.block_container);
            this.blockHeightTextView = (TextView) itemView.findViewById(R.id.block_row_chain_height);
            this.blockHashTextView = (TextView) itemView.findViewById(R.id.block_row_hash);
            this.blockTransactionsTextView = (TextView) itemView.findViewById(R.id.block_row_transactions_text);
            this.blockConfirmationsTextView = (TextView) itemView.findViewById(R.id.block_row_confirmations_text);
            this.blockTimestampTextView = (TextView) itemView.findViewById(R.id.block_row_timestamp);
        }
    }
}