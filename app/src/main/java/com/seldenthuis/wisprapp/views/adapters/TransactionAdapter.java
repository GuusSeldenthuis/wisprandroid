package com.seldenthuis.wisprapp.views.adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.seldenthuis.wisprapp.R;
import com.seldenthuis.wisprapp.domain.Transaction;

import java.text.SimpleDateFormat;
import java.util.List;

import static com.seldenthuis.wisprapp.views.fragments.TransactionsFragment.TAG;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder>
{

    private List<Transaction> transactions;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss dd-MM-yyyy");

    public TransactionAdapter(List<Transaction> transactions)
    {
        this.transactions = transactions;
    }

    public void setTransactions(List<Transaction> transactions)
    {
        this.transactions = transactions;
        this.notifyDataSetChanged();
    }

    public void addTransaction(Transaction transaction)
    {
        this.transactions.add(transaction);
        this.notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.block_transaction_row_view, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        Transaction transaction = this.transactions.get(position);
        holder.transactionBlockHash.setText(String.format("[%s..%s]", transaction.getBlockHash().substring(0, 6), transaction.getBlockHash().substring(58, 64)));
        holder.transactionTxid.setText(String.format("[%s..%s]", transaction.getId().substring(0, 6), transaction.getId().substring(58, 64)));
        Log.d(TAG, "onBindViewHolder: " + transaction.getConfirmations());
        holder.transactionConfirms.setText(String.valueOf(transaction.getConfirmations()));
        holder.transactionTime.setText(this.simpleDateFormat.format(transaction.getTime()));

//        Log.d(TAG, "onBindViewHolder: " + transaction);
        holder.itemView.setTag(transaction);
    }

    @Override
    public int getItemCount()
    {
        return this.transactions.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder
    {

        public TextView transactionBlockHash;
        public TextView transactionTxid;
        public TextView transactionTime;
        public TextView transactionConfirms;

        public ViewHolder(View itemView)
        {
            super(itemView);
            this.transactionBlockHash = (TextView) itemView.findViewById(R.id.block_transaction_row_block_hash_text_view);
            this.transactionTxid = (TextView) itemView.findViewById(R.id.block_transaction_row_hash_text_view);
            this.transactionConfirms = (TextView) itemView.findViewById(R.id.block_transaction_row_confirmations_text_view);
            this.transactionTime = (TextView) itemView.findViewById(R.id.block_transaction_row_timestamp_text_view);
        }
    }
}