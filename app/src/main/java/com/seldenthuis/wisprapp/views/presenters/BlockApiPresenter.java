package com.seldenthuis.wisprapp.views.presenters;

import com.seldenthuis.wisprapp.domain.Block;

public interface BlockApiPresenter
{

    void block(Block block);

    void blockHash(String blockHash);

    void blockHeight(int blockHeight);

    void onError(String errorMessage);

}
