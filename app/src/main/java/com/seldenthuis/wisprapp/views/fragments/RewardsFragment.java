package com.seldenthuis.wisprapp.views.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seldenthuis.wisprapp.R;
import com.seldenthuis.wisprapp.domain.Address;
import com.seldenthuis.wisprapp.domain.Block;
import com.seldenthuis.wisprapp.network.address.AddressApi;
import com.seldenthuis.wisprapp.network.block.BlockApi;
import com.seldenthuis.wisprapp.views.adapters.BlockAdapter;
import com.seldenthuis.wisprapp.views.presenters.AddressApiPresenter;
import com.seldenthuis.wisprapp.views.presenters.BlockApiPresenter;

public class RewardsFragment extends Fragment implements BlockApiPresenter, AddressApiPresenter
{

    public static String TAG = "seldenthuis";

    private BlockApi blockApi;

    private AddressApi addressApi;

    private RecyclerView rewardRecyclerView;

    private BlockAdapter rewardAdapter;

    private SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        this.blockApi = new BlockApi(this);
        this.addressApi = new AddressApi(this);
        this.getActivity().setTitle("Stake/MN rewards");

        this.sharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        String highScore = this.sharedPreferences.getString("savedRewardAddress", null);


        // Inflate the layout for this previewFragment
        return inflater.inflate(R.layout.fragment_overview,
                container,
                false);
    }

    @Override
    public void onViewCreated(View parent, Bundle savedInstance)
    {
        this.setupBlockAdapter();
        this.rewardRecyclerView = parent.findViewById(R.id.blocks_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.rewardRecyclerView.setLayoutManager(llm);
        this.rewardRecyclerView.setAdapter(this.rewardAdapter);
    }

    private void setupBlockAdapter()
    {
//        this.rewardAdapter.setAdapterPositionListener(new AdapterPositionListener()
//        {
//            @Override
//            public void adapterItemPosition(int position)
//            {
//                if (RewardsFragment.this.rewardAdapter.getItemCount() - position < 5) {
//                    RewardsFragment.this.blockApi.getByBlockHash(
//                            RewardsFragment.this.blockChain.getTail().previousBlockHash
//                    );
//                }
//            }
//        });
//        this.rewardAdapter.addOnBlockClickedListener(new OnBlockClickedListener()
//        {
//            @Override
//            public void onClick(Block block)
//            {
//                Log.d(TAG, "onClick: " + block.toString());
//            }
//        });
    }

    @Override
    public void onStart()
    {
        super.onStart();

        // This starts the 'chain' of event by first fetching the highest block-index.
        this.blockApi.getBlockHeight();

//        refreshTimer.schedule(new TimerTask()
//        {
//            public void run()
//            {
//                Log.d(TAG, "run: Hufhfjh");
//            }
//        }, 0, 60 * 1000);
    }

    @Override
    public void block(final Block block)
    {
//        this.blockChain.add(block);
//        getActivity().runOnUiThread(new Runnable()
//        {
//            @Override
//            public void run()
//            {
//                RewardsFragment.this.rewardAdapter.setBlocks(RewardsFragment.this.blockChain);
//            }
//        });

    }

    @Override
    public void blockHash(String blockHash)
    {
        // After receiving the block-hash based on the current block-height we're able to fetch our first block.
        this.blockApi.getByBlockHash(blockHash);
    }

    @Override
    public void blockHeight(int blockHeight)
    {
        // After receiving the height we can fetch the latest block hash.
        this.blockApi.getBlockHashByIndex(blockHeight);
    }


    @Override
    public void address(Address address)
    {
        Log.d(TAG, "address: " + address.toString());
    }

    @Override
    public void onError(String errorMessage)
    {
        Log.d(TAG, "onError: " + errorMessage);
    }

}
