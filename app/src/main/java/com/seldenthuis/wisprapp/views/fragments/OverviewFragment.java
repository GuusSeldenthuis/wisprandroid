package com.seldenthuis.wisprapp.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.seldenthuis.wisprapp.R;
import com.seldenthuis.wisprapp.domain.Address;
import com.seldenthuis.wisprapp.domain.Block;
import com.seldenthuis.wisprapp.domain.BlockChain;
import com.seldenthuis.wisprapp.network.address.AddressApi;
import com.seldenthuis.wisprapp.network.block.BlockApi;
import com.seldenthuis.wisprapp.views.adapters.AdapterPositionListener;
import com.seldenthuis.wisprapp.views.adapters.BlockAdapter;
import com.seldenthuis.wisprapp.views.interfaces.OnBlockClickedListener;
import com.seldenthuis.wisprapp.views.presenters.AddressApiPresenter;
import com.seldenthuis.wisprapp.views.presenters.BlockApiPresenter;

import java.util.Timer;

public class OverviewFragment extends Fragment implements BlockApiPresenter, AddressApiPresenter
{

    public static String TAG = "seldenthuis";

    private BlockApi blockApi;

    private AddressApi addressApi;

    private RecyclerView blocksRecyclerView;

    private OnBlockClickedListener onBlockClickedListener;

    private BlockChain blockChain = new BlockChain();

    private BlockAdapter blockAdapter;

    Timer refreshTimer = new Timer();

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {

        this.blockApi = new BlockApi(this);
        this.addressApi = new AddressApi(this);
        this.getActivity().setTitle("Blockchain overview");
        // Inflate the layout for this previewFragment
        return inflater.inflate(R.layout.fragment_overview,
                container,
                false);
    }

    @Override
    public void onViewCreated(View parent, Bundle savedInstance)
    {
        this.setupBlockAdapter();
        this.blocksRecyclerView = parent.findViewById(R.id.blocks_recycler_view);

        LinearLayoutManager llm = new LinearLayoutManager(this.getContext());
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        this.blocksRecyclerView.setLayoutManager(llm);
        this.blocksRecyclerView.setAdapter(this.blockAdapter);
    }

    private void setupBlockAdapter()
    {
        this.blockAdapter = new BlockAdapter(this.blockChain);
        this.blockAdapter.setAdapterPositionListener(new AdapterPositionListener()
        {
            @Override
            public void adapterItemPosition(int position)
            {
                if (OverviewFragment.this.blockAdapter.getItemCount() - position < 5) {
                    OverviewFragment.this.blockApi.getByBlockHash(
                            OverviewFragment.this.blockChain.getTail().previousBlockHash
                    );
                }
            }
        });
        this.blockAdapter.addOnBlockClickedListener(new BlockIsClicked());
    }

    @Override
    public void onStart()
    {
        super.onStart();

        this.getActivity().setTitle("Wispr blockchain");

        // This starts the 'chain' of event by first fetching the highest block-index.
        this.blockApi.getBlockHeight();

//        refreshTimer.schedule(new TimerTask()
//        {
//            public void run()
//            {
//                Log.d(TAG, "run: Hufhfjh");
//            }
//        }, 0, 60 * 1000);
    }

    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);
        try {
            this.onBlockClickedListener = (OnBlockClickedListener) context;
        } catch (ClassCastException castException) {
            /** The activity does not implement the listener. */
            /** :O */
        }
    }

    @Override
    public void block(final Block block)
    {
        this.blockChain.add(block);
        getActivity().runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                OverviewFragment.this.blockAdapter.setBlocks(OverviewFragment.this.blockChain);
            }
        });

    }

    @Override
    public void blockHash(String blockHash)
    {
        // After receiving the block-hash based on the current block-height we're able to fetch our first block.
        this.blockApi.getByBlockHash(blockHash);
    }

    @Override
    public void blockHeight(int blockHeight)
    {
        // After receiving the height we can fetch the latest block hash.
        this.blockApi.getBlockHashByIndex(blockHeight);
    }


    @Override
    public void address(Address address)
    {
        Log.d(TAG, "address: " + address.toString());
    }

    @Override
    public void onError(String errorMessage)
    {
        Log.d(TAG, "onError: " + errorMessage);
    }

    public class BlockIsClicked implements OnBlockClickedListener
    {

        @Override
        public void onClick(Block block)
        {
            Log.d(TAG, "onClick: " + block.toString());
            OverviewFragment.this.onBlockClickedListener.onClick(block);
        }
    }

}
