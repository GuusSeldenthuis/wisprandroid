package com.seldenthuis.wisprapp.network;

import android.support.annotation.Nullable;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;

public abstract class Api
{

    protected final String API_BASE_URL = "https://explorer.wispr.tech/";
    protected final String API_URL = "api/";
    protected final String EXT_URL = "ext/";

    protected static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    @Nullable
    protected OkHttpClient okHttpClient;

    /**
     * Method for setting up the Api's okHttp client.
     */
    protected abstract void setup();

}
