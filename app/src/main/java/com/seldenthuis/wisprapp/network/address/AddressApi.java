package com.seldenthuis.wisprapp.network.address;

import com.seldenthuis.wisprapp.domain.AddressJsonMapper;
import com.seldenthuis.wisprapp.network.Api;
import com.seldenthuis.wisprapp.views.presenters.AddressApiPresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class AddressApi extends Api implements AddressRepository
{

    private AddressApiPresenter addressApiPresenter;

    private AddressJsonMapper addressJsonMapper;

    public AddressApi(AddressApiPresenter addressApiPresenter)
    {
        this.addressApiPresenter = addressApiPresenter;
        this.setup();
    }

    @Override
    protected void setup()
    {
        this.okHttpClient = new OkHttpClient();
        this.addressJsonMapper = new AddressJsonMapper();
    }

    @Override
    public void getByAddress(String address)
    {
        final Request request = new Request.Builder()
                .url(API_BASE_URL + EXT_URL + "getaddress/" + address)
                .get()
                .addHeader("Accept", "application/json")
                .build();

        try {
            this.okHttpClient.newCall(request).enqueue(new Callback()
            {
                @Override
                public void onFailure(Call call, IOException e)
                {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            AddressApi.this.addressApiPresenter.onError(jsonObject.toString());
                            return;
                        }

                        JSONObject jsonObject = new JSONObject(responseBody.string());

                        AddressApi.this.addressApiPresenter.address(AddressApi.this.addressJsonMapper.transform(jsonObject.toString()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException("Something went wrong while running " + this.getClass());
        }
    }
}
