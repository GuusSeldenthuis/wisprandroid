package com.seldenthuis.wisprapp.network.transaction;

import com.seldenthuis.wisprapp.domain.TransactionJsonMapper;
import com.seldenthuis.wisprapp.network.Api;
import com.seldenthuis.wisprapp.views.presenters.TransactionApiPresenter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class TransactionApi extends Api implements TransactionRepository
{

    private TransactionApiPresenter transactionApiPresenter;

    private TransactionJsonMapper transactionJsonMapper;

    public TransactionApi(TransactionApiPresenter transactionApiPresenter)
    {
        this.transactionApiPresenter = transactionApiPresenter;
        this.setup();
    }

    @Override
    protected void setup()
    {
        this.okHttpClient = new OkHttpClient();
        this.transactionJsonMapper = new TransactionJsonMapper();
    }

    @Override
    public void getById(String txid)
    {
        final Request request = new Request.Builder()
                .url(API_BASE_URL + API_URL + "getrawtransaction?txid=" + txid + "&decrypt=1")
                .get()
                .addHeader("Accept", "application/json")
                .build();

        try {
            this.okHttpClient.newCall(request).enqueue(new Callback()
            {

                @Override
                public void onFailure(Call call, IOException e)
                {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            TransactionApi.this.transactionApiPresenter.onError(jsonObject.toString());
                            return;
                        }

                        JSONObject jsonObject = new JSONObject(responseBody.string());

                        TransactionApi.this.transactionApiPresenter.transaction(TransactionApi.this.transactionJsonMapper.transform(jsonObject.toString()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException("Something went wrong while running " + this.getClass());
        }
    }
}
