package com.seldenthuis.wisprapp.network.block;

import android.util.Log;

import com.seldenthuis.wisprapp.domain.BlockJsonMapper;
import com.seldenthuis.wisprapp.network.Api;
import com.seldenthuis.wisprapp.views.presenters.BlockApiPresenter;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

import static com.seldenthuis.wisprapp.views.fragments.TransactionsFragment.TAG;

public class BlockApi extends Api implements BlockRepository
{

    private BlockApiPresenter blockApiPresenter;

    private BlockJsonMapper blockJsonMapper;

    public BlockApi(BlockApiPresenter blockApiPresenter)
    {
        this.blockApiPresenter = blockApiPresenter;
        this.setup();
    }

    @Override
    protected void setup()
    {
        this.okHttpClient = new OkHttpClient();
        this.blockJsonMapper = new BlockJsonMapper();
    }

    @Override
    public void getByBlockHash(String blockHash)
    {
        Log.d(TAG, "getByBlockHash: " + blockHash);
        final Request request = new Request.Builder()
                .url(API_BASE_URL + API_URL + "getblock?hash=" + blockHash)
                .get()
                .addHeader("Accept", "application/json")
                .build();

        try {
            this.okHttpClient.newCall(request).enqueue(new Callback()
            {
                @Override
                public void onFailure(Call call, IOException e)
                {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            BlockApi.this.blockApiPresenter.onError(jsonObject.toString());
                            return;
                        }

                        BlockApi.this.blockApiPresenter.block(BlockApi.this.blockJsonMapper.transform(responseBody.string()));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException("Something went wrong while running " + this.getClass());
        }
    }

    @Override
    public void getBlockHashByIndex(int index)
    {
        final Request request = new Request.Builder()
                .url(API_BASE_URL + API_URL + "getblockhash?index=" + index)
                .get()
                .addHeader("Accept", "application/json")
                .build();

        try {
            this.okHttpClient.newCall(request).enqueue(new Callback()
            {
                @Override
                public void onFailure(Call call, IOException e)
                {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            BlockApi.this.blockApiPresenter.onError(jsonObject.toString());
                            return;
                        }

                        BlockApi.this.blockApiPresenter.blockHash(responseBody.string());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException("Something went wrong while running " + this.getClass());
        }
    }

    @Override
    public void getBlockHeight()
    {
        final Request request = new Request.Builder()
                .url(API_BASE_URL + API_URL + "getblockcount")
                .get()
                .addHeader("Accept", "application/json")
                .build();

        try {
            this.okHttpClient.newCall(request).enqueue(new Callback()
            {
                @Override
                public void onFailure(Call call, IOException e)
                {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException
                {
                    try (ResponseBody responseBody = response.body()) {
                        if (!response.isSuccessful()) {
                            JSONObject jsonObject = new JSONObject(responseBody.string());
                            BlockApi.this.blockApiPresenter.onError(jsonObject.toString());
                            return;
                        }

                        // return;
                        int blockHeight = Integer.parseInt(responseBody.string());

                        BlockApi.this.blockApiPresenter.blockHeight(blockHeight);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });

        } catch (Exception e) {
            System.out.println(e.toString());
            throw new RuntimeException("Something went wrong while running " + this.getClass());
        }
    }
}
