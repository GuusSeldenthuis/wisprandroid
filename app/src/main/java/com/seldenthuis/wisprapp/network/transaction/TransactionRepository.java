package com.seldenthuis.wisprapp.network.transaction;

public interface TransactionRepository
{

    void getById(String txid);

}
