package com.seldenthuis.wisprapp.network.address;

public interface AddressRepository
{

    void getByAddress(String address);

}
