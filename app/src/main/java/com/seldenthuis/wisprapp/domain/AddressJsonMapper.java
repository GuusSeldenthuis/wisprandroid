package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seldenthuis.wisprapp.domain.Address;

import java.io.IOException;

public class AddressJsonMapper
{

    protected ObjectMapper objectMapper;

    public AddressJsonMapper()
    {
        this.setupObjectMapper();
    }

    private void setupObjectMapper()
    {
        this.objectMapper = new ObjectMapper();
        objectMapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
    }

    public Address transform(String jsonString) throws IOException
    {
        return objectMapper.readValue(jsonString, Address.class);
    }

}
