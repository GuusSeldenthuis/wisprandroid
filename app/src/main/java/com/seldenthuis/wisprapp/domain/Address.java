package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Address
{
    private final String address;

    private final int sent;

    private final int received;

    private ArrayList<TransactionMeta> lastTransactions;

    @JsonCreator
    public Address(
            @JsonProperty("address") String address,
            @JsonProperty("sent") int sent,
            @JsonProperty("received") int received,
            @JsonProperty("last_txs") ArrayList<TransactionMeta> lastTransactions)

    {
        this.address = address;
        this.sent = sent;
        this.received = received;
        this.lastTransactions = lastTransactions;
    }

    public String getAddress()
    {
        return this.address;
    }

    public int getSent()
    {
        return this.sent;
    }

    public int getReceived()
    {
        return this.received;
    }

    public ArrayList<TransactionMeta> getLastTransactions()
    {
        return this.lastTransactions;
    }

    @Override
    public String toString()
    {
        return "Address{" +
                "address='" + this.address + '\'' +
                ", sent=" + this.sent +
                ", received=" + this.received +
                ", lastTrans actions=" + this.lastTransactions +
                '}';
    }
}
