package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Transaction
{
    private final String id;

    private final String blockHash;

    private final int confirmations;

    private final Date time;

    @JsonCreator
    public Transaction(
            @JsonProperty("txid") String id,
            @JsonProperty("blockhash") String blockHash,
            @JsonProperty("confirmations") int confirmations,
            @JsonProperty("time") long time)
    {
        this.id = id;
        this.blockHash = blockHash;
        this.confirmations = confirmations;
        this.time = new Date(time * 1000);
    }

    public String getId()
    {
        return id;
    }

    public String getBlockHash()
    {
        return blockHash;
    }

    public int getConfirmations()
    {
        return confirmations;
    }

    public Date getTime()
    {
        return time;
    }

    @Override
    public String toString()
    {
        return "Transaction{" +
                "id='" + id + '\'' +
                ", blockHash='" + blockHash + '\'' +
                ", confirmations=" + confirmations +
                ", time=" + time +
                '}';
    }
}
