package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.seldenthuis.wisprapp.domain.enums.TransactionType;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionMeta
{
    private final String id;

    private final TransactionType transactionType;

    @JsonCreator
    public TransactionMeta(
            @JsonProperty("addresses") String id,
            @JsonProperty("type") String type)
    {
        this.id = id;
        this.transactionType = TransactionType.get(type);
    }

    public String getId()
    {
        return id;
    }

    public TransactionType getTransactionType()
    {
        return transactionType;
    }

    @Override
    public String toString()
    {
        return "TransactionMeta{" +
                "id='" + id + '\'' +
                ", transactionType=" + transactionType +
                '}';
    }
}
