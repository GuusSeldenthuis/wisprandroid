package com.seldenthuis.wisprapp.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;
import java.util.Date;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RawTransaction
{
    private final String hex;

    private final String txId;

    private final int version;

    private final Date locktime;

    private Value[] valueIn;

    private Value[] valoutOut;

    private final String blockHash;

    private final int confirmations;

    private final Date time;

    private final Date blocktime;

    @JsonCreator
    public RawTransaction(
            @JsonProperty("hex") String hex,
            @JsonProperty("txid") String txId,
            @JsonProperty("version") int version,
            @JsonProperty("locktime") long locktime,
            @JsonProperty("blockhash") String blockHash,
            @JsonProperty("confirmations") int confirmations,
            @JsonProperty("time") long time,
            @JsonProperty("blocktime") long blocktime
            )
    {
        this.hex = hex;
        this.txId = txId;
        this.version = version;
        this.locktime = new Date(locktime * 1000);
        this.blockHash = blockHash;
        this.confirmations = confirmations;
        this.time = new Date(time * 1000);
        this.blocktime = new Date(blocktime * 1000);
    }

    public String getHex()
    {
        return hex;
    }

    public String getTxId()
    {
        return txId;
    }

    public int getVersion()
    {
        return version;
    }

    public Date getLocktime()
    {
        return locktime;
    }

    public Value[] getValueIn()
    {
        return valueIn;
    }

    public Value[] getValoutOut()
    {
        return valoutOut;
    }

    public String getBlockHash()
    {
        return blockHash;
    }

    public int getConfirmations()
    {
        return confirmations;
    }

    public Date getTime()
    {
        return time;
    }

    public Date getBlocktime()
    {
        return blocktime;
    }

    @Override
    public String toString()
    {
        return "RawTransaction{" +
                "hex='" + hex + '\'' +
                ", txId='" + txId + '\'' +
                ", version=" + version +
                ", locktime=" + locktime +
                ", valueIn=" + Arrays.toString(valueIn) +
                ", valoutOut=" + Arrays.toString(valoutOut) +
                ", blockHash='" + blockHash + '\'' +
                ", confirmations=" + confirmations +
                ", time=" + time +
                ", blocktime=" + blocktime +
                '}';
    }
}
